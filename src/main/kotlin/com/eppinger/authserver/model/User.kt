package com.eppinger.authserver.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.data.annotation.Id

data class User (
        @Id @JsonProperty("id") val id: String? = null,
        @JsonProperty("username") val username: String? = null,
        @JsonProperty("role") val role: String? = "user",
        @JsonIgnore val hashedPass: String? = null
)
