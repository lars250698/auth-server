package com.eppinger.authserver.model

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.data.annotation.Id

data class Client(
        @Id @JsonProperty("id") val id: String? = null,
        @JsonProperty("client_name") val clientName: String? = null,
        @JsonProperty("client_description") val clientDescription: String? = null
)
