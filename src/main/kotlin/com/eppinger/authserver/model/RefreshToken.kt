package com.eppinger.authserver.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.data.annotation.Id

data class RefreshToken (
        @Id @JsonIgnore val id: String? = null,
        @JsonProperty("refresh_token") val refreshToken: String,
        @JsonIgnore val valid: Boolean = false
)
