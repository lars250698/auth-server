package com.eppinger.authserver.model

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * AccessToken type
 * @param accessToken Access token for resources on the resource server
 * @param tokenType Type of access token. Defaults to Bearer
 * @param expiresIn Time to access token expiry. Optional
 * @param refreshToken Token to refresh the access token. Optional
 * @param scope Validity scope of access token. Optional
 */
data class AccessToken (
        @JsonProperty("access_token") val accessToken: String,
        @JsonProperty("token_type") val tokenType: String,
        @JsonProperty("expires_in", required = false) val expiresIn: Long? = null,
        @JsonProperty("refresh_token", required = false) val refreshToken: String? = null,
        @JsonProperty("scope", required = false) val scope: String? = null
)

