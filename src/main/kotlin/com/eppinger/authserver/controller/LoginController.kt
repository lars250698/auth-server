package com.eppinger.authserver.controller

import com.eppinger.authserver.exception.InvalidRequestException
import com.eppinger.authserver.model.Client
import com.eppinger.authserver.service.ClientService
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
@RequestMapping("/authorize")
class LoginController(
        val clientService: ClientService
) {

    /**
     * Login for Implicit Flow
     * @param responseType
     * @param clientId
     * @param redirectUrl
     */
    @GetMapping(params = ["response_type", "client_id", "redirect_url"])
    fun authorizeImplicitFlow(
            @RequestParam(name = "response_type") responseType: String,
            @RequestParam(name = "client_id") clientId: String,
            @RequestParam(name = "redirect_url") redirectUrl: String,
            model: Model
    ): String {
        assertResponseOrGrantType("token", responseType)
        // val client = clientService.getById(clientId) ?: throw InvalidRequestException()
        val client = Client(null, "testClient")
        model.addAttribute("client", client)
        return "login"
    }

    /**
     * Login for Authorization Code Flow
     * @param responseType
     * @param clientId
     * @param codeChallenge
     * @param codeChallengeMethod
     * @param redirectUrl
     */
    @GetMapping(params = ["response_type", "client_id", "code_challenge", "code_challenge_method", "redirect_url"])
    fun authorizeAuthorizationCodeFlow(
            @RequestParam(name = "response_type") responseType: String,
            @RequestParam(name = "client_id") clientId: String,
            @RequestParam(name = "code_challenge") codeChallenge: String,
            @RequestParam(name = "code_challenge_method") codeChallengeMethod: String,
            @RequestParam(name = "redirect_url") redirectUrl: String
    ) {
        assertResponseOrGrantType("code", responseType)
    }

    /**
     * Login for Resource Owner Password Credentials Flow
     * @param username The user's username
     * @param password The user's password
     * @param grantType
     * @param clientId
     * @param clientSecret
     */
    @GetMapping(params = ["grant_type", "username", "password", "client_id", "client_secret"])
    fun authorizeResourceOwnerPasswordCredentialsFlow(
            @RequestParam(name = "grant_type") grantType: String,
            @RequestParam(name = "username") username: String,
            @RequestParam(name = "password") password: String,
            @RequestParam(name = "client_id") clientId: String,
            @RequestParam(name = "client_secret") clientSecret: String
    ) {
        assertResponseOrGrantType("password", grantType)
    }

    /**
     * Login for Client Credentials Flow
     * @param grantType
     * @param clientId,
     * @param clientSecret
     */
    @GetMapping(params = ["grant_type", "client_id", "client_secret"])
    fun authorizeClientCredentialsFlow(
            @RequestParam(name = "grant_type") grantType: String,
            @RequestParam(name = "client_id") clientId: String,
            @RequestParam(name = "client_secret") clientSecret: String
    ) {
        assertResponseOrGrantType("client_credentials", grantType)
    }

    @ExceptionHandler(InvalidRequestException::class)
    fun invalidLoginRequestException() {

    }

    fun assertResponseOrGrantType(expected: String, actual: String) {
        if (expected != actual) {
            throw InvalidRequestException()
        }
    }
}
