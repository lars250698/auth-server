package com.eppinger.authserver.exception

class EntityNotFoundException: Exception()
