package com.eppinger.authserver.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.thymeleaf.spring4.SpringTemplateEngine
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver
import org.thymeleaf.spring4.view.ThymeleafViewResolver
import org.thymeleaf.templatemode.TemplateMode

@Configuration
class ThymeleafConfiguration {

    @Bean
    public fun templateEngine(): SpringTemplateEngine {
        return SpringTemplateEngine().apply {
            this.setTemplateResolver(thymeleafTemplateResolver())
        }
    }

    @Bean
    public fun thymeleafTemplateResolver(): SpringResourceTemplateResolver {
        return SpringResourceTemplateResolver().apply {
            this.prefix = "/WEB-INF/views/"
            this.suffix = ".html"
            this.templateMode = TemplateMode.HTML
        }
    }

    @Bean
    public fun viewResolver(): ThymeleafViewResolver {
        return ThymeleafViewResolver().apply {
            this.templateEngine = templateEngine()
        }
    }
}
