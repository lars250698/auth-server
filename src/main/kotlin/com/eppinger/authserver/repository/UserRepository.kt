package com.eppinger.authserver.repository

import com.eppinger.authserver.model.User
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.*

/**
 * Interface for database operations for User type.
 * Automatically implemented by Spring
 */
interface UserRepository : MongoRepository<User, String> {

    fun findByUsername(username: String): Optional<User>
}
