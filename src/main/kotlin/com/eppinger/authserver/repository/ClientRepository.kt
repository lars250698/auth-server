package com.eppinger.authserver.repository

import com.eppinger.authserver.model.Client
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.*

/**
 * Interface for database operations for Client type.
 * Automatically implemented by Spring
 */
interface ClientRepository: MongoRepository<Client, String> {

    fun findByClientName(clientName: String): Optional<Client>
}
