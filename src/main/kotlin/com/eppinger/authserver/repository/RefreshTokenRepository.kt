package com.eppinger.authserver.repository

import com.eppinger.authserver.model.RefreshToken
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.*

/**
 * Interface for database operations for RefreshToken type.
 * Automatically implemented by Spring
 */
interface RefreshTokenRepository: MongoRepository<RefreshToken, String> {

    fun getByRefreshToken(refreshToken: String): Optional<RefreshToken>
}
