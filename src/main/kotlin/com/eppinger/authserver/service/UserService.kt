package com.eppinger.authserver.service

import com.eppinger.authserver.exception.EntityNotFoundException
import com.eppinger.authserver.model.User
import com.eppinger.authserver.repository.UserRepository
import org.apache.logging.log4j.util.Strings
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

/**
 * Database operations for User type
 * @param userRepository Injected by Spring
 */
@Service
class UserService(val userRepository: UserRepository): AbstractCrudService<String, User> {
    override fun getAll(pageable: Pageable): Page<User> = userRepository.findAll(pageable)

    override fun getById(id: String): User? = userRepository.findById(id).orElse(null)

    override fun insert(obj: User): User = userRepository.insert(obj)

    @Throws(EntityNotFoundException::class)
    override fun update(obj: User): User {
        return if (userRepository.existsById(obj.id ?: Strings.EMPTY)) {
            userRepository.save(obj)
        } else {
            throw EntityNotFoundException()
        }
    }

    override fun deleteById(id: String): User? {
        return userRepository.findById(id).apply {
            this.ifPresent { userRepository.delete(it) }
        }.orElse(null)
    }

    fun getByUsername(username: String) = userRepository.findByUsername(username)
}
