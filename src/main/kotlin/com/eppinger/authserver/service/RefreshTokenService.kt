package com.eppinger.authserver.service

import com.eppinger.authserver.exception.EntityNotFoundException
import com.eppinger.authserver.model.RefreshToken
import com.eppinger.authserver.repository.RefreshTokenRepository
import org.apache.logging.log4j.util.Strings
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

/**
 * Database operations for RefreshToken type
 * @param refreshTokenRepository Injected by Spring
 */
@Service
class RefreshTokenService(val refreshTokenRepository: RefreshTokenRepository): AbstractCrudService<String, RefreshToken> {
    override fun getAll(pageable: org.springframework.data.domain.Pageable): Page<RefreshToken> = refreshTokenRepository.findAll(pageable)

    override fun getById(id: String): RefreshToken? = refreshTokenRepository.findById(id).orElse(null)

    override fun insert(obj: RefreshToken): RefreshToken = refreshTokenRepository.insert(obj)

    @Throws(EntityNotFoundException::class)
    override fun update(obj: RefreshToken): RefreshToken {
        return if (refreshTokenRepository.existsById(obj.id ?: Strings.EMPTY)) {
            refreshTokenRepository.save(obj)
        } else {
            throw EntityNotFoundException()
        }
    }

    override fun deleteById(id: String): RefreshToken? {
        return refreshTokenRepository.findById(id).apply {
            this.ifPresent { refreshTokenRepository.delete(it) }
        }.orElse(null)
    }

    fun getByRefreshtoken(refreshToken: String) = refreshTokenRepository.getByRefreshToken(refreshToken).orElse(null)
}
