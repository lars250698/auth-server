package com.eppinger.authserver.service

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

/**
 * Defines basic database operations
 */
interface AbstractCrudService<K,T> {
    fun getAll(pageable: Pageable): Page<T>
    fun getById(id: K): T?
    fun insert(obj: T): T
    fun update(obj: T): T
    fun deleteById(id: K): T?
}
