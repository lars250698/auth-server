package com.eppinger.authserver.service

import com.eppinger.authserver.exception.EntityNotFoundException
import com.eppinger.authserver.model.Client
import com.eppinger.authserver.repository.ClientRepository
import org.apache.logging.log4j.util.Strings
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

/**
 * Database operations for Client type
 * @param clientRepository Injected by Spring
 */
@Service
class ClientService(val clientRepository: ClientRepository) : AbstractCrudService<String, Client> {
    override fun getAll(pageable: Pageable): Page<Client> = clientRepository.findAll(pageable)

    override fun getById(id: String): Client? = clientRepository.findById(id).orElse(null)

    override fun insert(obj: Client): Client = clientRepository.insert(obj)

    @Throws(EntityNotFoundException::class)
    override fun update(obj: Client): Client {
        return if (clientRepository.existsById(obj.id ?: Strings.EMPTY)) {
            clientRepository.save(obj)
        } else {
            throw EntityNotFoundException()
        }
    }

    override fun deleteById(id: String): Client? {
        return clientRepository.findById(id).apply {
            this.ifPresent { clientRepository.delete(it) }
        }.orElse(null)
    }

    fun getByClientname(clientname: String): Client? = clientRepository.findByClientName(clientname).orElse(null)
}
